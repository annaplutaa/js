class Account{
    constructor(number, balance, currency) {
        this.number = number
        this.balance = balance
        this.currency = currency
    }
}

class Person {
    constructor(firstName, lastName, accountsList){
        this.firstName = firstName
        this.lastName = lastName
        this.accountsList = accountsList 
    }

    calculateBalance() {
        var saldo = 0;
        for(var i = 0; i < this.accountsList.length; i++){
            saldo += this.accountsList[i].balance;
        }
        return saldo;
    } 

    sayHello() {
        return 'Hello ' + this.firstName + ' ' + this.lastName + ',\n' + 'number of accounts: ' + this.accountsList.length + 
        '\n' + 'total balance: ' + this.calculateBalance() + '\n'; 
    }

    addAccount(newAccount) { 
        this.accountsList.push(newAccount);
    } 

    findAccount(accountNumber) {
        return this.accountsList.find(elementTable => elementTable.number === accountNumber);
    }


    withdraw(accountNumber, amount) {
        return new Promise((resolve, reject) => {
            var foundAccount = this.findAccount(accountNumber);
            if(foundAccount && amount <= foundAccount.balance && amount > 0){
                foundAccount.balance -= amount;
                setTimeout(function() {
                    resolve(`Number of account: ${accountNumber}, new balance: ${foundAccount.balance} ${foundAccount.currency}, withdrawn: ${amount} ${foundAccount.currency}`);
                }, 3000);
            }
            else if (!foundAccount) {
                reject('The account can\'t be found');
            }
            else if (amount > foundAccount.balance) {
                reject('The requested amount is bigger than account balance');
            }
            else if (amount <= 0) {
                reject('Wrong amount')
            }
            else {
                reject('Incorrect value')
            }

        });
    
    }
}


var person = new Person("Anna", "Pluta", [new Account(1, 20000, "PLN"), new Account(2, 30004, "PLN")]);

console.log(person.sayHello());
person.addAccount(new Account(3, 'a9', 'PLN'));
console.log(person.sayHello());

//console.log(person.findAccount(2));

person.withdraw(3, 99)
    .then(function (resolve){
        console.log(resolve);
    })
    .catch(function (reject) {
        console.log(reject);
    });
