var personFactory = function() {
    var details = {
        firstName: 'Anna',
        lastName: 'Pluta',
        accounstList: [
            {balance: 200050, currency: 'PLN'},
            {balance: 3500600, currency: 'PLN'}]
    }

    return {
        firstName: details.firstName,
        lastName: details.lastName,

        sayHello: function() {
            return 'Hello ' + this.firstName + ' ' + this.lastName + ',\n' + 'number of accounts: ' + details.accounstList.length;
        } 

    }

} 

var myObject = personFactory(); 
console.log(myObject.sayHello()); 