var person = (function() {
    var details = {
        firstName: 'Anna',
        lastName: 'Pluta',
        accountsList: [
            {balance: 200050, currency: 'PLN'},
            {balance: 3500600, currency: 'PLN'}]
    }; 

    function calculateBalance() {
        var saldo = 0;
        for(var i = 0; i < details.accountsList.length; i++) {
            saldo += details.accountsList[i].balance;
        }
        return saldo;
    }; 
    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accountsList: details.accountsList, 

        sayHello: function() {
            return 'Hello ' + this.firstName + ' ' + this.lastName + ',\n' + 'number of accounts: ' + this.accountsList.length + 
            '\n' + 'total balance: ' + calculateBalance() + '\n'; 
        }, 

        addAccount: function(balance, currency) { 
            this.accountsList.push({balance: balance, currency: currency}) 
        } 

    }; 

})(); 

console.log(person.sayHello());
person.addAccount(1, 'PLN');
console.log(person.sayHello());
