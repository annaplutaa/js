const init = (() => {

    class Account{
        constructor(number, balance, currency) {
            this.number = number
            this.balance = balance
            this.currency = currency
        }
    }

    class Person {
        constructor(firstName, lastName, accountsList){
            this.firstName = firstName
            this.lastName = lastName
            this.accountsList = accountsList 
        }

        addAccount(newAccount) { 
            this.accountsList.push(newAccount);
        }
        
        findAccount(accountNumber) {
            return this.accountsList.find(elementTable => elementTable.number === accountNumber);
        }


        withdraw(accountNumber, amount) {
            return new Promise((resolve, reject) => {
                var foundAccount = this.findAccount(accountNumber);
                if(foundAccount && amount <= foundAccount.balance && amount > 0){
                    foundAccount.balance -= amount;
                    setTimeout(function() {
                        resolve(`Number of account: ${accountNumber}, new balance: ${foundAccount.balance} ${foundAccount.currency}, withdrawn: ${amount} ${foundAccount.currency}`);
                    }, 3000);
                }
                else if (!foundAccount) {
                    reject('The account can\'t be found');
                }
                else if (amount > foundAccount.balance) {
                    reject('The requested amount is bigger than account balance');
                }
                else if (amount <= 0) {
                    reject('Wrong amount')
                }
                else {
                    reject('Incorrect value')
                }

            });
        
        }
    }

    

    document.addEventListener('DOMContentLoaded', function () {
        
        const myPerson = new Person("Anna", "Pluta", [new Account(1, 20000, "PLN"), new Account(2, 30004, "PLN")]);

        var name = document.querySelector('.card-title');
        var account = document.querySelector('.card-text');
        var fieldNumber = document.getElementById('number');
        var fieldAmount = document.getElementById('amount');
        var button = document.querySelector('.btn.btn-primary');
        var message = document.getElementById('message');

        function onClicked() {
            name.innerHTML = `${myPerson.firstName} ${myPerson.lastName}`;

            function card(){
                for(var i = 0; i < myPerson.accountsList.length; i++) {
                    var accounts = `Number: ${myPerson.accountsList[i].number} balance: ${myPerson.accountsList[i].balance} currency: ${myPerson.accountsList[i].currency } \n`;
                    account.innerHTML += '<p>' + accounts + '</p>';
                    
                };
            };
            card();

        
            function withdrawHTML () {
                var accountNumber = parseInt(fieldNumber.value);
                var amount = parseFloat(fieldAmount.value);

                myPerson.withdraw(accountNumber, amount)
                    .then((resolve) => {
                        account.innerHTML = '<p> Accounts: </p>';
                        card();
                        message.innerText = resolve;
                    })
                    .catch((reject) => {
                        message.innerText = reject;
                    });
                    
            }

            button.addEventListener('click', withdrawHTML);
        }
        function onChanged(){
            button.disabled = true;
            fieldAmount.addEventListener('change', () => {
                if(fieldNumber.value && fieldAmount.value) {
                    button.disabled = false;
                } else {
                    button.disabled = true;
                }
            });
            fieldNumber.addEventListener('change', () => {
                if(fieldNumber.value && fieldAmount.value) {
                    button.disabled = false;
                } else {
                    button.disabled = true;
                }
            });
            
        }


       return {
           onClicked: onClicked(),
           onChanged: onChanged()
        }
    }); 
}) (); 
