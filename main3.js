function Account(ballance, currency) {
    this.balance = ballance,
    this.currency = currency
    
}

var person = (function() {
    var details = {
        firstName: 'Anna',
        lastName: 'Pluta',
        accountsList: [
            new Account(200050, 'PLN'),
            new Account(3500600, 'PLN')
        ], 
    }; 

    function calculateBalance() {
        var saldo = 0;
        for(var i = 0; i < details.accountsList.length; i++){
            saldo += details.accountsList[i].balance;
        }
        return saldo;
    }; 
    
    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accountsList: details.accountsList, 

        sayHello: function() {
            return 'Hello ' + this.firstName + ' ' + this.lastName + ',\n' + 'number of accounts: ' + this.accountsList.length + 
            '\n' + 'total balance: ' + calculateBalance() + '\n'; 
        },

        addAccount: function(newAccount) { 
            this.accountsList.push(newAccount)
        } 

    }; 

})(); 

console.log(person.sayHello());
person.addAccount(new Account(99, 'PLN'));
console.log(person.sayHello());

